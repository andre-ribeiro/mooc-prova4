
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author André Luis Ribeiro
 */
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException {
        Scanner s = new Scanner(System.in);
        
        /// "src/main/resources/lancamentos.txt"
        ProcessaLancamentos pl = new ProcessaLancamentos(s.nextLine());
        
        List<Lancamento> ll = pl.getLancamentos();
        
        
        boolean entradaErrada = true;
        int conta = -1;
        while(true) {
            if (s.hasNextInt()) {
                conta = s.nextInt();
                if(conta == 0) break;
                Pratica.exibeLancamentosConta(ll, conta);
            } else {
                System.out.println("Por favor, informe um valor numérico");
                s.next();
            }
        }
        
        
        
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
        Lancamento l = new Lancamento(conta, null, null, null);
        int i = lancamentos.indexOf(l);
        if(i == -1) {
            System.out.println("Conta inexistente");
            return;
        }
        boolean continua = true;
        
        while(continua) {
            System.out.println(lancamentos.get(i));
            i++;
            if(i >= lancamentos.size() || !lancamentos.get(i).equals(l))
                continua = false;
        }
    }
 
}