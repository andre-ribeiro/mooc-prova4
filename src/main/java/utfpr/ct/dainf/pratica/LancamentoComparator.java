package utfpr.ct.dainf.pratica;

import java.util.Comparator;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparator<Lancamento> {

    @Override
    public int compare(Lancamento t, Lancamento t1) {
        return t.getConta() - t1.getConta() + t.getData().compareTo(t1.getData());
    }
    
}
