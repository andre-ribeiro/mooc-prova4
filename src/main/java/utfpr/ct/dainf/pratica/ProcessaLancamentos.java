package utfpr.ct.dainf.pratica;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Linguagem Java
 * @author
 */
public class ProcessaLancamentos {
    private BufferedReader reader;

    public ProcessaLancamentos(File arquivo) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(arquivo));
    }

    public ProcessaLancamentos(String path) throws FileNotFoundException {
        this.reader = new BufferedReader(new FileReader(path));
    }
    
    private String getNextLine() throws IOException {
        return reader.readLine();
    }
    
    private Lancamento processaLinha(String linha) {
        // Lancamento l = new Lancamento();
        if(linha == null) return null;
        Integer conta = Integer.valueOf(linha.substring(0, 6));
        String sData = linha.substring(6, 14);
        Date dData = new GregorianCalendar(Integer.valueOf(sData.substring(0, 4)), 
                                           Integer.valueOf(sData.substring(4, 6))-1, 
                                           Integer.valueOf(sData.substring(6,8))).getTime();
        
        Double d = Double.valueOf(linha.substring(74, 86))/100;
        return new Lancamento(conta, dData, linha.substring(14, 74).trim(), d);
    }
    
    private Lancamento getNextLancamento() throws IOException {
        return processaLinha(getNextLine());
    }
    
    public List<Lancamento> getLancamentos() throws IOException {
        List<Lancamento> lancamentos = new ArrayList<>();
        Lancamento l = null;
        do {
            l = getNextLancamento(); 
            if(l != null)
                lancamentos.add(l);
        } while(l != null);
        Collections.sort(lancamentos, new LancamentoComparator());
        reader.close();
        return lancamentos;
    }
    
}
